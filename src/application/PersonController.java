package application;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class PersonController implements PropertyChangeListener { 
	
 private Person model;
 
 @FXML TextField nameField;
 @FXML Label nameFieldRule;
 @FXML TextField emailField;
 
 public void initialize () {
	 setModel(new Person("Thauanne" ,"mottathauanne@gmail.com"));
	 updateNameView();
	 emailField.setText(model.getEmail());
 }
	private void setModel(Person person) {
		if (this.model != null){	
			this.model.removePropertyChangeListener(this);
		}
		this.model=person;
		if (this.model != null) {
			this.model.addPropertyChangeListener(this);
		}
	}
		private void updateNameView() {
			nameField.setText(model.getName());
		}
		
		@FXML 
		void nameFieldChange(ObservableValue<? extends String> property, String oldValue, String newValue) {
		validateNameView(newValue);

		}
	private void validateNameView(String newValue) {
			validate (newValue, "[\\\\p{L}\\\\.\\\\-\\\\s)]*", nameField, nameFieldRule );
			
		}
	@FXML
	void nameFieldFocusChange(ObservableValue<? extends Boolean > property, Boolean oldValue, Boolean newValue) {
		if (! newValue) {
			try {
				updateNameModel();
			} catch (Exception e) {
				
			}
		}
			}
	

	private void updateNameModel() {
		model.setName(nameField.getText());
		
	}
	
	@FXML void emailFieldChange(ObservableValue <? extends String> property, String oldValue, String newValue) {
		validate(newValue, "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",emailField, null);
	}
	private void validate(String newValue, String string, TextField nameField2, Label nameFieldRule2) {
		boolean isValid =  newValue.matches(string);
		String color = isValid ? "white" : "red";
		nameField.setStyle("-fx-background-color:" + color);
		if(nameFieldRule2 != null) {
			nameFieldRule2.setVisible(! isValid);
		}
		
		
	}
	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
 if (arg0.getSource() != model) {
	 return;
 }
 String propertyName = arg0.getPropertyName();
 if (Person.NAME_PROPERTY.equals(propertyName)) {
	 updateNameView();
 } else if (Person.EMAIL_PROPERTY.equals(propertyName)) {
		
	}

}
}
